import org.emerjoin.titan.wire.annotation.Lifespan;
import org.emerjoin.titan.wire.annotation.Tag;
import org.emerjoin.titan.wire.object.scope.Application;

@Lifespan(Application.class)
public class ApplicationScopedExample {

    private MyObject myObject;

    public ApplicationScopedExample(@Tag("example1") MyObject myObject){
        this.myObject = myObject;
    }

}
