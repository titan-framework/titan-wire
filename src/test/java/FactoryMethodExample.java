import org.emerjoin.titan.wire.annotation.Factory;
import org.emerjoin.titan.wire.annotation.Tag;

public class FactoryMethodExample {

    @Factory @Tag("example1")
    public MyObject create1(){
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

    @Factory @Tag("example2")
    public MyObject create2(){
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

}
