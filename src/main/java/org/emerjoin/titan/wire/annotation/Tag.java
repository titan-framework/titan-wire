package org.emerjoin.titan.wire.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines a keyword to be used to distinguish an object instance from another of the same type.
 * @author Mário Júnior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD , ElementType.TYPE})
public @interface Tag {
    String value();
}
