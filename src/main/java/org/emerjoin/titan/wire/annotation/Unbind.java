package org.emerjoin.titan.wire.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation should be used on a binding Managed object to identify the method to be invoked to unbind a Managed object instance previously bound.
 * @author Mário Júnior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Unbind {

}
