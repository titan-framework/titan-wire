package org.emerjoin.titan.wire.annotation;

import org.emerjoin.titan.wire.object.ObjectInstanceScope;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the scope to which a Managed Object instance lifetime is bound.
 * @author Mário Júnior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Lifespan {
    Class<? extends ObjectInstanceScope> value();
}
