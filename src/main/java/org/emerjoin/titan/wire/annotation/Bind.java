package org.emerjoin.titan.wire.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Indicates that all instances of a specific Managed object type must be bound to another Managed object.
 * This annotation should be used on the binding Managed object to identify the method to be invoked to perform the bind.
 * @author Mário Júnior
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Bind {

}
