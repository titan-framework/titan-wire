package org.emerjoin.titan.wire.spi;

import org.emerjoin.titan.wire.WiringContext;
import org.emerjoin.titan.wire.intecept.InterceptorBinding;

public interface WiringContextFactory {

    WiringContext createContext(ObjectScopeProvider[] scopeProviders, InterceptorBinding[] interceptorBindings);

}
