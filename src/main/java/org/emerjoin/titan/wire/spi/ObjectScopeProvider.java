package org.emerjoin.titan.wire.spi;

import org.emerjoin.titan.wire.object.ObjectScopeContext;

public interface ObjectScopeProvider {

    void validate(Class type);
    ObjectScopeContext getContext();

}
