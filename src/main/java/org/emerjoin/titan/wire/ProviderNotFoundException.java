package org.emerjoin.titan.wire;

public class ProviderNotFoundException extends WiringException {

    public ProviderNotFoundException() {
        super("No wiring provider was found");
    }

}
