package org.emerjoin.titan.wire;

import org.emerjoin.titan.wire.object.ObjectFactory;

import java.util.Optional;
import java.util.Set;

public interface WiringContext {

    <T> Optional<T> resolveObject(Class<T> type);
    <T> Optional<T> resolveObject(Class<T> type, String tag);
    <T> Set<T> resolveObjectSet(Class<T> type);
    void registerManagedObjectType(Class managedObjectType);
    void registerObjectFactory(ObjectFactory objectFactory);

    void unregisterManagedObjectType(Class managedObjectType);
    void unregisterObjectFactory(ObjectFactory objectFactory);

}
