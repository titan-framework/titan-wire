package org.emerjoin.titan.wire.intecept;

import java.lang.annotation.Annotation;

public class InterceptorBinding {

    private Class<? extends MethodInterceptor> interceptorType;
    private Class<? extends Annotation> annotation;

    public InterceptorBinding(Class<? extends MethodInterceptor> interceptorType, Class<? extends Annotation> annotation){
        this.interceptorType = interceptorType;
        this.annotation = annotation;
    }

    public Class<? extends MethodInterceptor> getInterceptorType() {
        return interceptorType;
    }

    public Class<? extends Annotation> getAnnotation() {
        return annotation;
    }

}
