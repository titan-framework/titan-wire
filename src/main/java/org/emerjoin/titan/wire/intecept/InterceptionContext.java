package org.emerjoin.titan.wire.intecept;

import org.emerjoin.titan.wire.object.scope.Method;

public interface InterceptionContext {
    Method getMethod();
    Object[] getParameters();
    Object proceed(Object... parameters) throws Exception;
}
