package org.emerjoin.titan.wire.intecept;

@FunctionalInterface
public interface MethodInterceptor {

    Object intercept(InterceptionContext context) throws Exception;

}
