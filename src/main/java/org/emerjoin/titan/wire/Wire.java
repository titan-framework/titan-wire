package org.emerjoin.titan.wire;

import org.emerjoin.titan.wire.intecept.InterceptorBinding;
import org.emerjoin.titan.wire.spi.ObjectScopeProvider;
import org.emerjoin.titan.wire.spi.WiringContextFactory;

import java.util.Iterator;
import java.util.ServiceLoader;

public final class Wire {

    private static WiringContext WIRING_CONTEXT;
    private static WiringContextFactory WIRING_CONTEXT_FACTORY;

    private static void initialize(){
        Iterator<WiringContextFactory> iterator = ServiceLoader.load(WiringContextFactory.class)
                .iterator();
        if(!iterator.hasNext())
            throw new ProviderNotFoundException();
        WIRING_CONTEXT_FACTORY = iterator.next();
    }

    public static void initialize(InterceptorBinding[] interceptorBindings, ObjectScopeProvider[] scopeProviders) throws WiringException {
        if(WIRING_CONTEXT_FACTORY==null)
            initialize();
        WIRING_CONTEXT = WIRING_CONTEXT_FACTORY.createContext(scopeProviders,
                interceptorBindings);
    }

    public static WiringContext currentContext(){
        if(WIRING_CONTEXT==null)
            throw new NoActiveContextException();
        return WIRING_CONTEXT;
    }

}
