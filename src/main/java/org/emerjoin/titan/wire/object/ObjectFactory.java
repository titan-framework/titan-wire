package org.emerjoin.titan.wire.object;


import java.util.Optional;
import java.util.Set;

/**
 * Creates object Instances.
 * @author Mário Júnior
 */
public interface ObjectFactory {

    Optional<Object> getObject(Class type, InstanceCreationContext context);
    Set<Object> getObjects(Class type, InstanceCreationContext context);

}
