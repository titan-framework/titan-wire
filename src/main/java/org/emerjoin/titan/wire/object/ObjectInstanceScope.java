package org.emerjoin.titan.wire.object;


/**
 * Represents the scope to which a Managed object instance lifetime is bound.
 * @author Mário Júnior
 */
public interface ObjectInstanceScope {

}
