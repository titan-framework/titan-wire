package org.emerjoin.titan.wire.object.scope;

import org.emerjoin.titan.wire.object.ObjectInstanceScope;

public interface Dependent extends ObjectInstanceScope {

}
