package org.emerjoin.titan.wire.object;


import java.util.Optional;
import java.util.Set;

public interface ObjectScopeContext {

    <T> Set<T> getObjectSet(Class<T> objectType);
    <T> Optional<T> getObject(Class<T> objectType);
    <T> Optional<T> getObject(Class<T> objectType, String tag);

}
