package org.emerjoin.titan.wire.object;

import java.util.Optional;
import java.util.Set;

/**
 * Provide information regarding the creation of Managed object instance.
 */
public interface InstanceCreationContext {
    Class getType();
    Class<? extends ObjectInstanceScope> getScope();
    <T> Set<T> resolveObjectSet(Class<T> type);
    <T> Optional<T> resolveObject(Class<T> type);
}
