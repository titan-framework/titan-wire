package org.emerjoin.titan.wire.object.scope;

import org.emerjoin.titan.wire.object.ObjectInstanceScope;

/**
 * @author Mário Júnior
 */
public interface WebSession extends ObjectInstanceScope {
    
}
