package org.emerjoin.titan.wire;

public class NoActiveContextException extends WiringException {

    public NoActiveContextException() {
        super("There is no active wiring context");
    }
}
